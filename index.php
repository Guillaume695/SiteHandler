<!DOCTYPE html>
<html lang="fr">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0"/>
	<title>Home</title>
	<meta name="theme-color" content="#6692bd">
	<link rel="icon" href="siteHandler/img/design/logo-small-min.png">

	<!-- CSS  -->	
	<link href="siteHandler/css/font-awesome.min.css" type="text/css" rel="stylesheet" >
	<link href="siteHandler/css/materialize.css" type="text/css" rel="stylesheet"/>
	<link href="siteHandler/css/style.css" type="text/css" rel="stylesheet"/>
	<link href="siteHandler/css/input.css" type="text/css" rel="stylesheet"/>
	<link href="siteHandler/css/button.css" type="text/css" rel="stylesheet"/>
	<link href="siteHandler/css/icon.css" type="text/css" rel="stylesheet"/>
</head>
<body style="background: #efefef;">
	
	<div class="row" style="margin-top: 3em; margin-bottom: 0px;">
		<div class="col s12 center-align">
			<img src="siteHandler/img/design/logo-min.png" class="responsive-img center-img" style="width: 5em;">
			<h3>SiteHandler (1.0)</h3>
		</div>
	</div>

	<div class="row" style="margin-bottom: 0.5em;">
		<div class="col s12 m12 l10 offset-l1">
			<form id="search_form">
				<div class="ui action input">
					<input id="input" type="text" placeholder="Rechercher">
					<button type="submit" class="ui icon button">
						<i class="search icon"></i>
					</button>
				</div>
			</form>
		</div>
	</div>

	<div class="row" style="margin-bottom: 1em;">
		<div class="col s12 m12 l10 offset-l1" style="border: 1px solid #cccccc; padding-top: 1em; border-radius: 0.2em; height: 25em; overflow-y: auto;">
		<?php
			$folderContent = scandir(".");
			array_shift($folderContent);
			array_shift($folderContent);
			foreach ($folderContent as $site) {
				if (!is_file($site)) {
					if ($site != 'html' && $site != 'siteHandler' && $site != 'SiteHandler') {
						if (is_file($site."/public_html/web/img/design/logo-min.png")) {
							$logo = $site."/public_html/web/img/design/logo-min.png";
						} else {
							$logo = "siteHandler/img/design/logo-min.png";
						}
						$creationDateTime = new DateTime();
						$creationDateTime->setTimestamp(filectime($site));
		?>

			<div data-site="<?php echo $site; ?>" class="site-card col s12 m6 l4" style="padding: 0.5em;">
				<a href="/<?php echo $site;?>/public_html/web/app_dev.php">
					<div class="col s12 website link-hover">
						<div class="col" style="max-width: 5em;">
							<img src="<?php echo $logo; ?>" class="responsive-img center-img">
						</div>
						<div class="col">
							<h5 style="margin-top: 0.2em; margin-bottom: 0px;"><?php echo ucfirst($site); ?></h5>
							<em style="color: #6b6b6b;">Date de création du projet : <?php echo $creationDateTime->format('d/m/Y'); ?></em>
						</div>
					</div>
				</a>
			</div>

		<?php
					}
				}
			}
		?>
		</div>
	</div>

	<div class="row" style="margin-bottom: 1em;">
		<div class="col s12 m6 offset-m3 l4 offset-l4 website" style="background-color: #efefef !important;">
			<div id="newWebsite" class="link-hover">
				<div class="col" style="max-width: 5em;">
					<svg enable-background="new 0 0 512 512" id="Layer_1" version="1.1" viewBox="0 0 512 512" width="2em" xml:space="preserve"><g><path d="M256.108,3.02c-139.743,0-253,113.257-253,253s113.257,252.995,253,252.995   c139.743,0,253-113.252,253-252.995S395.852,3.02,256.108,3.02z M256.108,488.775c-128.338,0-232.76-104.417-232.76-232.755   c0-128.339,104.422-232.76,232.76-232.76c128.338,0,232.76,104.421,232.76,232.76C488.868,384.358,384.446,488.775,256.108,488.775   z" fill="#6b6b6b"/><polygon fill="#6b6b6b" points="266.228,104.22 245.988,104.22 245.988,245.9 104.98,245.9 104.98,266.14 245.988,266.14    245.988,407.148 266.228,407.148 266.228,266.14 407.908,266.14 407.908,245.9 266.228,245.9  "/></g></svg>
				</div>
				<div class="col">
					<big><big style="color: #6b6b6b;">Nouveau site</big></big>
				</div>
			</div>
		</div>
		<div id="newWebsiteForm" class="col s12 m6 offset-m3 l4 offset-l4" style="display: none; margin-top: 1em;">
			<form method="POST" action="siteHandler/createNewWebsite.php">
				<div class="row" style="margin-bottom: 1em;">
					<div class="ui input">
						<span>Nom du site <sup><span class="red-text">*</span></sup></span>
						<input name="website" type="text" placeholder="SiteHandler" style="height: 2.5em; margin-top: -0.5em; margin-left: 1em;" required="_required">
					</div>
				</div>
				<div class="row" style="margin-bottom: 1em;">
					<div class="ui input">
						<span>Nom d'utilisateur (base de données SQL) <sup><span class="red-text">*</span></sup></span>
						<input name="username" type="text" placeholder="sitehandler2016" style="height: 2.5em; margin-top: -0.5em; margin-left: 1em;" required="_required">
					</div>
				</div>
				<div class="row" style="margin-bottom: 1em;">
					<div class="ui input">
						<span>Mot de passe (base de données SQL) <sup><span class="red-text">*</span></sup></span>
						<input name="password" type="password" placeholder="******" style="height: 2.5em; margin-top: -0.5em; margin-left: 1em;" required="_required">
					</div>
				</div>
				<div class="row center-align" style="margin-bottom: 1em;">
					<button type="submit" class="btn green-btn waves-effect waves-light"><i class="fa fa-check left"></i>Créer le site</button>
				</div>
			</form>
		</div>
	</div>

	
	<div class="row" style="margin-bottom: 1em;">
		<a href="/phpmyadmin">
			<div class="col s12 m6 offset-m3 l4 offset-l4 link-hover website-phpmyadmin">
				<div class="col" style="width: 6em;">
					<img src="siteHandler/img/phpmyadmin.png" class="responsive-img center-img">
				</div>
				<div class="col">
					<h5><span style="color: #656598;">Php</span><span style="color: #ff9800">MyAdmin</span></h5>
				</div>
			</div>
		</a>
	</div>

	<div class="row center-align grey-text" style="margin-top: 5em;">
		© 2016 GUILLAUME FOURNIER
	</div>

	<script src="siteHandler/js/jquery-2.2.2.js"></script>
	<script src="siteHandler/js/materialize.js"></script>

	<script>
		function clean_overlay() {
			//La fonction est lancée tout de suite après le click donc, on compte le nombre de lean-overlay; s'il y en a plus qu'un
			//ceux en trop ne seront pas supprimés donc on intervient, sinon on laisse les choses se faire.
			while ($('.lean-overlay').length >= 2) {
				$('.lean-overlay').first().remove();
			}
		}

		$(document).ready(function() {

			$('#search_form').submit(function(e) {
				e.preventDefault();
				var elem = $('#input').val().toLowerCase();
				$('.site-card').each(function() {
					var site = $(this).attr('data-site');
					if (!site.toLowerCase().includes(elem)) {
						$(this).hide();
					} else {
						$(this).show();
					}
				});
			});

			$('#newWebsite').click(function() {
				if (!$('#newWebsiteForm').is(':visible')) {
					$('#newWebsiteForm').show();
				} else {
					$('#newWebsiteForm').hide();
				}
			});
		});
	</script>
  </body>
</html>