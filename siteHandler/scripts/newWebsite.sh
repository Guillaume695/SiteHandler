#!/bin/bash
nomSite=$1
username=$2
password=$3
nomSiteRez="$nomSite.rez.conf"
mkdir /var/www/$nomSite
cd scripts/

#Création du site de base
cp -R baseWebsite/* /var/www/$nomSite

#Copie et edition du fichier de config
cp modeleConfApache /var/www/$nomSite/$nomSiteRez
sed -i -e "s/#siteName?/$nomSite/g" /var/www/$nomSite/$nomSiteRez

#Edition du script SQL
cp /var/www/siteHandler/scripts/modeleSQL /var/www/siteHandler/scripts/SQLscript
sed -i -e "s/#siteName?/$username/g" /var/www/siteHandler/scripts/SQLscript
sed -i -e "s/#user?/$username/g" /var/www/siteHandler/scripts/SQLscript
sed -i -e "s/#password?/$password/g" /var/www/siteHandler/scripts/SQLscript
#Lancement de MySQL et du script
mysql --defaults-extra-file=/var/www/siteHandler/scripts/mysqlConf < /var/www/siteHandler/scripts/SQLscript
mv /var/www/siteHandler/scripts/SQLscript /var/www/$nomSite/SQLscript

chmod 755 -R /var/www/$nomSite

exit 0;